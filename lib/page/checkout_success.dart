import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';

class CheckoutSuccessPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    PreferredSizeWidget header() {
      return AppBar(
        backgroundColor: sepertiabuabu,
        centerTitle: true,
        title: Text(
          "Checkout Sukses",
          style: warnahitamstyle,
        ),
        elevation: 0,
      );
    }

    Widget content() {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset("assets/keranjang.png",
            width: 80,),
            SizedBox(
              height: 20,
            ),
            Text(
              "Anda telah melakukan transaksi",
              style: warnahitamstyle.copyWith(
                fontWeight: medium,
                fontSize: 16
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              "Terimakasih sudah \n melakukan transaksi",
              style: warnahitamstyle.copyWith(
                fontWeight: light,
                fontSize: 13
              ),
              textAlign: TextAlign.center,
            ),
            Container(
              width: 196,
              height: 44,
              margin: EdgeInsets.only(
                top: defaultMargin
              ),
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(context, "/home", (route) => false);
                },
                style: TextButton.styleFrom(
                  backgroundColor: secondarycolor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)
                  )
                ),
                child: Text(
                  "Beli lagi",
                  style: putihbangetstyle.copyWith(
                    fontSize: 15,
                    fontWeight: bold
                  ),
                ),
              ),
            ),
            Container(
              width: 196,
              height: 44,
              margin: EdgeInsets.only(
                  top: defaultMargin
              ),
              child: TextButton(
                onPressed: () {},
                style: TextButton.styleFrom(
                    backgroundColor: sepertiabuabu,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)
                    )
                ),
                child: Text(
                  "Lihat Keranjang saya",
                  style: warnautama.copyWith(
                      fontSize: 15,
                      fontWeight: bold
                  ),
                ),
              ),
            )


          ],
        ),
      );
    }

    return Scaffold(
      appBar: header(),
      body: content(),
    );
  }
}
