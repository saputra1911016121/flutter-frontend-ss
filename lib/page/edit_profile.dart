import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';

class EditProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    PreferredSizeWidget header(){
      return AppBar(
        leading: IconButton(
          icon: Icon(Icons.close,color: texthitam,),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: sepertiabuabu,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "Edit Profile",
          style: warnahitamstyle ,
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.check, color: secondarycolor,),
            onPressed: (){},
          )
        ],
      );
    };

    Widget nameInput() {
      return Container(
        margin: EdgeInsets.only(
            top: 30
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Name",
            style: warnahitamstyle.copyWith(
              fontSize: 13
            ),),
            TextFormField(
              style: warnautama,
              decoration: InputDecoration(
                hintText: "Bahrul",
                hintStyle: abuabustyle,
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: sepertiabuabu
                  )
                )
              ),
            )
          ],
        ),
      );
    }

    Widget username() {
      return Container(
        margin: EdgeInsets.only(
            top: 30
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Username",
              style: warnahitamstyle.copyWith(
                  fontSize: 13
              ),),
            TextFormField(
              style: warnautama,
              decoration: InputDecoration(
                  hintText: "@Bahrul",
                  hintStyle: abuabustyle,
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: sepertiabuabu
                      )
                  )
              ),
            )
          ],
        ),
      );
    }

    Widget emailinput() {
      return Container(
        margin: EdgeInsets.only(
            top: 30
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Email",
              style: warnahitamstyle.copyWith(
                  fontSize: 13
              ),),
            TextFormField(
              style: warnautama,
              decoration: InputDecoration(
                  hintText: "Bahrulilmi@gmail.com",
                  hintStyle: abuabustyle,
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: sepertiabuabu
                      )
                  )
              ),
            )
          ],
        ),
      );
    }

    Widget content() {
      return Container(
        padding: EdgeInsets.symmetric(
          horizontal: defaultMargin
        ),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 100,
              height: 100,
              margin: EdgeInsets.only(
                top: defaultMargin
              ),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage(
                    "assets/avatar.png"
                  )
                )
              ),
            ),
            nameInput(),
            username(),
            emailinput()
          ],
        ),
      );
    }



    return Scaffold(
      appBar: header(),
      body: content(),
      resizeToAvoidBottomInset: false,

    );
  }
}
