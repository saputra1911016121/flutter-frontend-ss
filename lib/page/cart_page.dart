import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';
import 'package:onlineshop/widgets/cart_card.dart';

class CartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    PreferredSizeWidget header() {
      return AppBar(
        backgroundColor: sepertiabuabu,
        centerTitle: true,
        title: Text(
          "Belanja",
          style: warnahitamstyle,
        ),
        elevation: 0,

      );
    }

    Widget emptyCart() {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "assets/keranjang.png",
              width: 80,
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "Keranjang Kosong",
              style: warnautama.copyWith(
                fontWeight: medium,
                fontSize: 16
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              "Cari Barang",
              style: abuabustyle,
            ),
            Container(
              width: 154,
              height: 44,
              margin: EdgeInsets.only(
                top: 20
              ),
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(context, "/home", (route) => false);
                },
                style: TextButton.styleFrom(
                  backgroundColor: secondarycolor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)
                  )
                ),
                child: Text(
                  "Cari Barang",
                  style: putihbangetstyle.copyWith(
                    fontSize: 16,
                    fontWeight: medium
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }

        Widget content() {
          return ListView(
            padding: EdgeInsets.symmetric(
              horizontal: defaultMargin
            ),
            children: [
              CartCard()
            ],
          );
        }
        Widget customBottomNav() {
        return Container(
          height: 175,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: defaultMargin
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Total",
                      style: warnahitamstyle,
                    ),
                    Text(
                      "Rp. 100.000",
                      style: pricetextstyle.copyWith(
                        fontWeight: semibold,
                        fontSize: 16
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Divider(
                thickness: 0.3,
                color: texthitam,
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                height: 50,
                margin: EdgeInsets.symmetric(
                  horizontal: defaultMargin
                ),
                child: TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/checkout");
                  },
                  style: TextButton.styleFrom(
                    backgroundColor: secondarycolor,
                    padding: EdgeInsets.symmetric(
                      horizontal: 20
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)
                    )
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Checkout", style: putihbangetstyle,),
                      Icon(Icons.arrow_forward, color: putihbanget,)
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
        }

    return Scaffold(
      appBar: header(),
      body: content(),
      bottomNavigationBar: customBottomNav()
    );
  }
}
