import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';
import 'package:onlineshop/widgets/checkout_card.dart';

class CheckoutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    PreferredSizeWidget header() {
      return AppBar(
        backgroundColor: sepertiabuabu,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "Checkout Details",
          style: warnahitamstyle.copyWith(
            fontWeight: medium
          ),
        ),
      );
    }

    Widget content() {
      return ListView(
        padding: EdgeInsets.symmetric(
          horizontal: defaultMargin
        ),
        children: [
          //Text
          SizedBox(
            height: 5,
          ),
          Container(
            margin: EdgeInsets.only(
              top: defaultMargin
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text (
                  "Item List",
                  style: warnahitamstyle.copyWith(
                    fontWeight: medium,
                    fontSize: 16
                  ),
                ),
                CheckoutCard(),
              ],
            ),
          ),

          //Alamat
          Container(
            margin: EdgeInsets.only(
              top: defaultMargin
            ),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: sepertiabuabu,
              borderRadius: BorderRadius.circular(12)
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Alamat Detail",
                  style: warnahitamstyle.copyWith(
                    fontSize: 16,
                    fontWeight: medium
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Row(
                  children: [
                    Column(
                      children: [
                        Image.asset("assets/tujuan.png",
                        width: 40,),
                        Image.asset("assets/line.png", height: 20,),
                        Image.asset("assets/lokasi.png", width: 40,)
                      ],
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Lokasi Toko",
                          style: warnautama.copyWith(
                            fontWeight: light,
                            fontSize: 12
                          ),
                        ),
                        Text(
                          "Bahrul Store",
                          style: warnahitamstyle.copyWith(
                            fontWeight: medium
                          ),
                        ),
                        SizedBox(height: defaultMargin,),
                        Text(
                          "Tujuan Pengiriman",
                          style: warnautama.copyWith(
                              fontWeight: light,
                              fontSize: 12
                          ),
                        ),
                        Text(
                          "Pekalongan, Jawa Tengah",
                          style: warnahitamstyle.copyWith(
                              fontWeight: medium
                          ),
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
          ),

          // PPembayaran
          Container(
            margin: EdgeInsets.only(
                top: defaultMargin
            ),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: sepertiabuabu,
                borderRadius: BorderRadius.circular(12)
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Pembayaran",
                  style: warnahitamstyle.copyWith(
                    fontWeight: medium,
                    fontSize: 16
                  ),
                ),
                SizedBox(height: 12 ,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Jumlah Product",
                      style: warnautama.copyWith(
                          fontWeight: medium
                      ),
                    ),
                    Text(
                      "2 Item",
                      style: warnahitamstyle.copyWith(
                          fontWeight: light
                      ),
                    ),

                  ],
                ),
                SizedBox(height: 12 ,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Pembayaran",
                      style: warnautama.copyWith(
                          fontWeight: medium
                      ),
                    ),
                    Text(
                      "Rp. 200.000",
                      style: warnahitamstyle.copyWith(
                          fontWeight: light
                      ),
                    ),

                  ],
                ),
                SizedBox(height: 12 ,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Ongkir",
                      style: warnautama.copyWith(
                          fontWeight: medium
                      ),
                    ),
                    Text(
                      "Gratis",
                      style: warnahitamstyle.copyWith(
                          fontWeight: light
                      ),
                    ),

                  ],
                ),
                SizedBox(height: 10,),
                Divider(
                  thickness: 1,
                  color: texthitam,
                ),
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Total Pembayaran",
                      style: warnautama.copyWith(
                          fontWeight: bold
                      ),
                    ),
                    Text(
                      "Rp.200.000",
                      style: warnautama.copyWith(
                          fontWeight: bold
                      ),
                    ),

                  ],
                )
              ],

            ),
          ),
          //Button Checkout
          SizedBox(
            height: defaultMargin,
          ),
          Divider(
            thickness: 1,
            color: texthitam,
          ),
          Container(
            height: 50,
            width: double.infinity,
            margin: EdgeInsets.symmetric(
              vertical: defaultMargin
            ),
            child: TextButton(
              onPressed: () {
                Navigator.pushNamedAndRemoveUntil(context, "/checkout-success", (route) => false);
              },
              style: TextButton.styleFrom(
                backgroundColor: secondarycolor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12)
                )
              ),
              child: Text(
                "Bayar Sekarang",
                style: putihbangetstyle.copyWith(
                  fontSize: 16,
                  fontWeight: semibold
                ),
              ),
            ),
          )

        ],
      );
    }

    return Scaffold(
      backgroundColor: putihbanget,
      appBar: header(),
      body: content(),
    );
  }
}
