import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ProductPage extends StatefulWidget {
  @override
  State<ProductPage> createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  List images = [
    "assets/sepatu.png",
    "assets/sepatu.png",
    "assets/sepatu.png",
  ];

  List familiarshoes = [
    "assets/sepatu.png",
    "assets/sepatu.png",
    "assets/sepatu.png",
    "assets/sepatu.png",
    "assets/sepatu.png",
    "assets/sepatu.png",
  ];

  int currentIndex = 0;
  bool isWishList = false;

  @override
  Widget build(BuildContext context) {

    Future<void> showSuccesDialog() async {
      return showDialog(
        context: context,
        builder: (BuildContext context) => Container(
          width: MediaQuery.of(context).size.width - (2 * defaultMargin),
          child: AlertDialog(
            backgroundColor: putihbanget,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            content: SingleChildScrollView(
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.centerRight,
                    child: GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.close,
                        color: texthitam,
                      ),
                    ),
                  ),
                  Image.asset(
                    "assets/sukses.png",
                    width: 90,
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Sukses Masukkan keranjang",
                        style: warnautama.copyWith(
                          fontSize: 16,
                          fontWeight: semibold
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        "Barang berhasil ditambahkan",
                        style: abuabustyle,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        width: 154,
                        height: 54,
                        child: TextButton(
                          onPressed: () {},
                          style: TextButton.styleFrom(
                            backgroundColor: secondarycolor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12)
                            )
                          ),
                          child: Text(
                            "Keranjang",
                            style: putihbangetstyle.copyWith(
                              fontSize: 15,
                              fontWeight: medium
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        )
      );
    }

    Widget indikatorgambar(int index) {
      return Container(
        width: currentIndex == index ? 16 : 4,
        height: 4,
        margin: EdgeInsets.symmetric(horizontal: 2),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: currentIndex == index ? secondarycolor : Color(0xffC4C4C4)),
      );
    }

    Widget familiarShoesCard(String imageUrl) {
      return Container(
        width: 54,
        height: 54,
        margin: EdgeInsets.only(
          right: 16
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(imageUrl)
          ),
          borderRadius: BorderRadius.circular(6)
        ),
      );
    }

    Widget header() {
      int index = -1;

      return Column(
        children: [
          Container(
            margin: EdgeInsets.only(
                top: 20, left: defaultMargin, right: defaultMargin),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.chevron_left,
                  ),
                ),
                Icon(
                  Icons.shopping_bag,
                  color: texthitam,
                )
              ],
            ),
          ),
          CarouselSlider(
            items: images
                .map(
                  (image) => Image.asset(
                    image,
                    width: MediaQuery.of(context).size.width,
                    height: 300,
                    fit: BoxFit.cover,
                  ),
                )
                .toList(),
            options: CarouselOptions(
                initialPage: 0,
                onPageChanged: (index, reason) {
                  setState(() {
                    currentIndex = index;
                  });
                }),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: images.map((e) {
              index++;
              return indikatorgambar(index);
            }).toList(),
          ),
        ],
      );
    }

    Widget content() {

      int index = -1;

      return Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 17),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(24)),
            color: secondarycolor),
        child: Column(
          children: [
            //Header
            Container(
              margin: EdgeInsets.only(
                  top: defaultMargin,
                  left: defaultMargin,
                  right: defaultMargin),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Nike Sport",
                          style: putihbangetstyle.copyWith(
                              fontSize: 18, fontWeight: semibold),
                        ),
                        Text(
                          "Olahraga",
                          style: putihbangetstyle.copyWith(fontSize: 12),
                        )
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isWishList = !isWishList;
                      });

                      if(isWishList){
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                                backgroundColor: pricecolor,
                                content: Text("Masuk ke Suka Item",
                                  textAlign: TextAlign.center,
                                ))
                        );
                      } else{
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                                backgroundColor: alertcolor,
                                content: Text("Hapus dari suka",
                                  textAlign: TextAlign.center,
                                ))
                        );
                      }

                    },
                    child: Image.asset(
                      isWishList ? "assets/suka2.png":"assets/suka.png",
                      width: 46,
                    ),
                  )
                ],
              ),
            ),

            //Price
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(
                  top: 20, left: defaultMargin, right: defaultMargin),
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                  color: sepertiabuabu, borderRadius: BorderRadius.circular(4)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Harga Barang",
                    style: warnahitamstyle,
                  ),
                  Text(
                    "Rp.100.000",
                    style: pricetextstyle.copyWith(
                        fontSize: 12, fontWeight: semibold),
                  )
                ],
              ),
            ),

            //Deskripsi
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(
                  top: defaultMargin,
                  left: defaultMargin,
                  right: defaultMargin),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Deskripsi Barang",
                    style: putihbangetstyle.copyWith(fontWeight: medium),
                  ),
                  SizedBox(height: 6,),
                  Text(
                      "Barang ini bagus banget rekomendasi buat kamu yang suka sepatu ini, dijamin kamu bakal suka",
                  style: putihbangetstyle.copyWith(
                    fontWeight: light
                  ),
                    textAlign: TextAlign.justify ,
                  )
                ],
              ),
            ),

            //Sepatu lainnya
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(
                top: defaultMargin
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: defaultMargin
                    ),
                    child: Text(
                      "Sepatu Lainnya",
                      style: putihbangetstyle.copyWith(
                        fontWeight: medium
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 12,
                  ),
                  SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: familiarshoes
                        .map(
                        (image){
                          index++;
                          return Container(
                              margin: EdgeInsets.only(left:  index == 0 ? defaultMargin:0),
                              child: familiarShoesCard(image));
                        },
                    ) .toList(),
                    ),
                  )
                ],
              ),
            ),

            //Button Bawah
            Container(
              width: double.infinity,
              margin: EdgeInsets.all(
                defaultMargin
              ),
              child: Row(
                children: [
                  GestureDetector(
                    onTap:(){
                      Navigator.pushNamed(context, "/detail-chat");
                    },
                    child: Container(
                      width: 54,
                      height: 54,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(
                              "assets/buttonchat.png",
                            )
                          )
                        ),
                    ),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: Container(
                      height: 54,
                      child: TextButton(
                        onPressed: () {
                          showSuccesDialog();
                        },
                        style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12)
                          ),
                          backgroundColor: putihbanget
                        ),
                        child: Text(
                          "Masukkan Keranjang",
                          style: warnautama.copyWith(
                            fontWeight: semibold,
                            fontSize: 16
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      );
    }

    return Scaffold(
        backgroundColor: productbackground,
        body: ListView(
          children: [header(), content()],
        ));
  }
}
