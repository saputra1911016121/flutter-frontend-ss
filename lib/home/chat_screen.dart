import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';
import 'package:onlineshop/widgets/chat_tile.dart';
class ChatScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Widget header() {
      return AppBar(
        backgroundColor: sepertiabuabu,
        centerTitle: true,
        title: Text(
          "Pesan Support",
          style: warnahitamstyle.copyWith(
            fontWeight: medium,
            fontSize: 18
          ),
        ),
        elevation: 0,
        automaticallyImplyLeading: false,
      );
    }

    Widget emptychat(){
      return Expanded(
          child: Container(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.headset_mic_outlined,
                  color: secondarycolor,
                  size: 70,
                ),
                SizedBox(height: 15,),
                Text(
                  "Anda tidak memiliki pesan",
                  style: warnahitamstyle.copyWith(
                      fontSize: 16,
                      fontWeight: medium
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Kamu tidak memiliki transaksi",
                  style: abuabustyle,
                ),
                SizedBox(height: 20,),
                Container(
                  height: 44,
                  child: TextButton(
                    onPressed: () {},
                    style: TextButton.styleFrom(
                        padding: EdgeInsets.symmetric(
                            horizontal: 24,
                            vertical: 10
                        ),
                        backgroundColor: secondarycolor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                        )
                    ),
                    child: Text(""
                        "Belanja",
                      style: putihbangetstyle.copyWith(
                          fontWeight: medium,
                          fontSize: 15
                      ),),
                  ),
                )
              ],
            ),)
      );
    }

    Widget content() {
      return Expanded(child: Container(
        width: double.infinity,
        child: ListView(
          padding: EdgeInsets.symmetric(
            horizontal: defaultMargin,
          ),
          children: [
            ChatTile(),

          ],
        ),
      ));
    }

    return Column(
      children: [
        header(),
        content()
      ],
    );
  }
}
