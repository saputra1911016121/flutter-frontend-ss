import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';
import 'package:onlineshop/widgets/chat_buble.dart';
class DetailChat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    PreferredSize header() {
      return PreferredSize(
        preferredSize: Size.fromHeight(70),
        child: AppBar(
          backgroundColor: sepertiabuabu,
          centerTitle: false,
          title: Row(
            children: [
              Image.asset(
                'assets/logoshop.png',
                width: 50,
              ),
              SizedBox(
                width: 12,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Shoe Store',
                    style: warnahitamstyle.copyWith(
                      fontWeight: medium,
                      fontSize: 14,
                    ),
                  ),
                  Text(
                    'Online',
                    style: warnahitamstyle.copyWith(
                      fontWeight: light,
                      fontSize: 10,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }

    Widget productpreview() {
      return Container(
        width: 225,
        height: 74,
        margin: EdgeInsets.only(
          bottom: 20,
        ),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: sepertiabuabu,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(
            color: secondarycolor
          )
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: Image.asset(
                "assets/sepatu.png",
                width: 54,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Sepatu Nike",
                  style: warnahitamstyle,
                  overflow: TextOverflow.ellipsis,),
                  SizedBox(height: 5,),
                  Text("Rp. 100.000",
                  style: pricetextstyle.copyWith(
                    fontWeight: medium
                  ),),
                ],
              ),
            ),
            Image.asset("assets/buttonclose.png", width: 22,)
          ],
        ),
      );
    }

    Widget chatInput(){
      return Container(
        margin: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
            children:[
            productpreview(),
            Row(
            children: [
              Expanded(
                child: Container(
                  height: 45,
                  padding: EdgeInsets.symmetric(
                    horizontal: 16,
                  ),
                  decoration: BoxDecoration(
                    color: sepertiabuabu,
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Center(
                    child: TextFormField(
                      decoration: InputDecoration.collapsed(
                        hintText: "Tuliskan Pesan"
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(width: 20,),
              Image.asset("assets/kirim.png",
              width: 45 ,)
            ],
          ),
        ]
        ),

      );
    }

    Widget content() {
      return ListView(
        padding: EdgeInsets.symmetric(
          horizontal: defaultMargin
        ),
        children: [
          ChatBubble(isSender: true,
          text: "hay  gimaan barangnya ?",
          hasProduct: true,),
          ChatBubble(isSender: false,
            text: "ready kakak ?",)
        ],
      );
    }
    return Scaffold(
      backgroundColor: putihbanget,
      appBar: header(),
      bottomNavigationBar: chatInput(),
      body: content(),
    );
  }
}
