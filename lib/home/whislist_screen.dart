import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';
import 'package:onlineshop/widgets/whislist_card.dart';

class WhislistScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Widget header() {
      return AppBar(
        backgroundColor: sepertiabuabu,
        centerTitle: true,
        title: Text(
          "Favorit",
          style: warnahitamstyle.copyWith(
            fontWeight: medium
          ),
        ),
        elevation: 0,
        automaticallyImplyLeading: false,
      );
    }

    Widget emptywhislist() {
      return Expanded(
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "assets/love.png",
                color: secondarycolor,
                width: 74,
              ),
              SizedBox(height: 23,),
              Text("Kamu tidak memiliki favorit",
              style: warnahitamstyle.copyWith(
                fontSize: 16,
                fontWeight: medium
              ),),
              SizedBox(
                height: 12,
              ),
              Text(
                "Cari favorit kamu",
                style: abuabustyle.copyWith(),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                child: TextButton(onPressed: () {},
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.symmetric(
                        vertical: 10,
                        horizontal: 24
                      ),
                      backgroundColor: secondarycolor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)
                      )
                    ),
                    child: Text("Belanja",
                    style: putihbangetstyle.copyWith(
                      fontWeight: medium,
                      fontSize: 16
                    ),)),
              )

            ],
          ),
        ),
      );
    }

    Widget content() {
      return Expanded(
        child: Container(
          child: Container(
            child: ListView(
              padding: EdgeInsets.symmetric(
                horizontal: defaultMargin
              ),
              children: [
                WishListCard(),
                WishListCard()
              ],
            ),
          ),
        ) ,
      );
    }

    return Column(
      children: [
        header(),
        //emptywhislist(),
        content()
      ],
    );
  }
}
