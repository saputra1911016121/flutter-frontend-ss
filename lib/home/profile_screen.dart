import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Widget header() {
      return AppBar(
        backgroundColor: sepertiabuabu,
        automaticallyImplyLeading: false,
        elevation: 0,
        flexibleSpace: SafeArea(child:
        Container(
          padding: EdgeInsets.all(defaultMargin),
          child: Row(
            children: [
              ClipOval(
                child: Image.asset(
                  "assets/avatar.png",
                  width: 54,
                ),
              ),
              SizedBox(
                width: 16,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Hallo, Bahrul",
                    style: warnahitamstyle.copyWith(
                      fontWeight: semibold,
                      fontSize: 24
                    ),),
                    Text(
                      "Bahrul",
                      style: abuabustyle.copyWith(
                        fontSize: 16
                      ),
                    )
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamedAndRemoveUntil(context, "/login", (route) => false);
                    },
                    child: Image.asset(
                    "assets/exit.png",
                    width: 20,
                ),
                  ),
                ]
              )
            ],
          ),
        )),
      );
    }

    Widget menutItem(String text){
      return Container(
        margin: EdgeInsets.only(top: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(text, style: abuabustyle.copyWith(fontSize: 13),),
            Icon(
              Icons.chevron_right,
              color: sepertiabuabu,
            )
          ],
        ),
      );
    }

    Widget content() {
      return Expanded(
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(
            horizontal: defaultMargin
          ),
          decoration: BoxDecoration(
            color: putihbanget
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Text(
                "Akun",
                style: warnahitamstyle.copyWith(
                  fontSize: 16,
                  fontWeight: semibold
                ),
              ),
              GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, "/editprofile");
                  },
                  child: menutItem("Edit Profile")),
              menutItem("Keranjang"),
              menutItem("Bantuan"),
              SizedBox(
                height: 30,
              ),
              Text(
                "Pengaturan Umum",
                style: warnahitamstyle.copyWith(
                    fontSize: 16,
                    fontWeight: semibold
                ),
              ),
              menutItem("Privasi & Kebijakan "),
              menutItem("Layanan"),
              menutItem("Tentang Applikasi"),
            ],
          ),
        ),
      );
    }


    return Column(
      children: [
        header(),
        content()
      ],
    );
  }
}
