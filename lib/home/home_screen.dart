import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';
import 'package:onlineshop/widgets/product_card.dart';
import 'package:onlineshop/widgets/product_tile.dart';

class HomePage extends StatelessWidget {
  @override
  Widget header() {
    return Container(
      margin: EdgeInsets.only(
          top: defaultMargin, left: defaultMargin, right: defaultMargin),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Hallo, Bahrul",
                  style: warnahitamstyle.copyWith(
                      fontSize: 20, fontWeight: semibold),
                ),
                Text(
                  "@bahrul",
                  style: warnahitamstyle.copyWith(fontSize: 15),
                )
              ],
            ),
          ),
          Container(
            width: 54,
            height: 54,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(image: AssetImage("assets/avatar.png"))),
          )
        ],
      ),
    );
  }

  Widget categories() {
    return Container(
      margin: EdgeInsets.only(
        top: defaultMargin,
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            SizedBox(
              width: defaultMargin,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 12,
                vertical: 10,
              ),
              margin: EdgeInsets.only(right: 16),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12), color: secondarycolor),
              child: Text(
                ""
                "Semua Barang",
                style: warnaputih.copyWith(fontSize: 13, fontWeight: medium),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 12,
                vertical: 10,
              ),
              margin: EdgeInsets.only(right: 16),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(
                    color: secondarytextcolor,
                  ),
                  color: transparentColor),
              child: Text(
                "Olahraga",
                style: warnahitamstyle.copyWith(fontSize: 13, fontWeight: medium),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 12,
                vertical: 10,
              ),
              margin: EdgeInsets.only(right: 16),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(
                    color: secondarytextcolor,
                  ),
                  color: transparentColor),
              child: Text(
                "Latihan",
                style: warnahitamstyle.copyWith(fontSize: 13, fontWeight: medium),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 12,
                vertical: 10,
              ),
              margin: EdgeInsets.only(right: 16),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(
                    color: secondarytextcolor,
                  ),
                  color: transparentColor),
              child: Text(
                "Basket",
                style: warnahitamstyle.copyWith(fontSize: 13, fontWeight: medium),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 12,
                vertical: 10,
              ),
              margin: EdgeInsets.only(right: 16),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(
                    color: secondarytextcolor,
                  ),
                  color: transparentColor),
              child: Text(
                "Traveling",
                style: warnahitamstyle.copyWith(fontSize: 13, fontWeight: medium),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget popularproducttitle() {
    return Container(
      margin: EdgeInsets.only(
        top: defaultMargin,
        left: defaultMargin,
        right: defaultMargin
      ),
      child: Text(
        "Produk Populer",
        style: warnahitamstyle.copyWith(
          fontWeight: semibold,
          fontSize: 22
        ),
      ),
    );
  }

  Widget popularproduct() {
    return Container(
      margin: EdgeInsets.only(top:14),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            SizedBox(
              width: defaultMargin,
            ),
            Row(
              children: [
                ProductCard(),
                ProductCard(),
                ProductCard(),

              ],
            )
          ],
        ),
      ),
    );
  }

  Widget produkbarutitle() {
    return Container(
      margin: EdgeInsets.only(
          top: defaultMargin,
          left: defaultMargin,
          right: defaultMargin
      ),
      child: Text(
        "Produk Baru",
        style: warnahitamstyle.copyWith(
            fontWeight: semibold,
            fontSize: 22
        ),
      ),
    );
  }

  Widget produkbaru() {
    return Container(
      margin: EdgeInsets.only(
        top: 14
      ),
      child: Column(
        children: [
          ProductTile(),
          ProductTile(),
          ProductTile(),
          ProductTile(),
        ],
      ),
    );
  }

  Widget build(BuildContext context) {
    return ListView(
      children: [header(),
        categories(),
        popularproducttitle(),
        popularproduct(),
        produkbarutitle(),
        produkbaru()
      ],
    );
  }
}
