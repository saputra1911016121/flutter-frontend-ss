import 'package:flutter/material.dart';
import 'package:onlineshop/home/chat_screen.dart';
import 'package:onlineshop/home/home_screen.dart';
import 'package:onlineshop/home/profile_screen.dart';
import 'package:onlineshop/home/whislist_screen.dart';
import 'package:onlineshop/theme.dart';

class MainPage extends StatefulWidget {
  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    Widget cardButton() {
      return FloatingActionButton(onPressed: () {
        Navigator.pushNamed(context, "/cart");
      },
        backgroundColor: secondarycolor ,
        child: Icon(
          Icons.shopping_bag_outlined,
      ),
      );
    }

    Widget custombuttonnav(){
      return ClipRRect(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30)
        ),
        child: BottomAppBar(
          shape: CircularNotchedRectangle(),
          notchMargin: 10,
          clipBehavior: Clip.antiAlias,
          child: BottomNavigationBar(
            backgroundColor:primarytextcolor ,
            currentIndex: currentIndex,
            onTap: (value) {
              print(value);
              setState(() {
                currentIndex = value;
              });
            } ,
            type: BottomNavigationBarType.fixed,
            items: [
            BottomNavigationBarItem(icon: Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: Image.asset("assets/Home.png",
              width: 20,
                  color: currentIndex == 0 ? secondarycolor: Color(0xff808191)
              ),
            ),
              label: ""
            ),
            BottomNavigationBarItem(icon: Container(
            margin: EdgeInsets.only(top: 10, bottom: 10),
              child: Image.asset("assets/chat.png",
              width: 20,
                  color: currentIndex == 1 ? secondarycolor: Color(0xff808191)
              ),
            ),
                label: ""
            ),
              BottomNavigationBarItem(icon: Container(
                margin: EdgeInsets.only(top: 10, bottom: 10),
                child: Image.asset("assets/love.png",
                width: 20,
                    color: currentIndex == 2 ? secondarycolor: Color(0xff808191)
                ),
              ),
                  label: ""
              ),
              BottomNavigationBarItem(icon: Container(
                margin: EdgeInsets.only(top: 10, bottom: 10),
                child: Image.asset("assets/Profile.png",
                width: 20,
                    color: currentIndex == 3 ? secondarycolor: Color(0xff808191)
                ),
              ),
                  label: ""
              )
          ],),
        ),
      );
    }

    Widget body(){
      switch(currentIndex) {
        case 0:
          return HomePage();
          break;
        case 1:
          return ChatScreen();
          break;
        case 2:
          return WhislistScreen();
          break;
        case 3:
          return ProfileScreen();
          break;

        default:
          return HomePage();

      }
    }

    return Scaffold(
      floatingActionButton: cardButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: custombuttonnav(),
      body:body(),
    );
  }
}
