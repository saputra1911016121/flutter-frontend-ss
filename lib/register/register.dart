import 'package:flutter/material.dart';
import 'package:onlineshop/providers/auth_provider.dart';
import 'package:onlineshop/widgets/loading_button.dart';
import 'package:provider/provider.dart';
import '../theme.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController nameController = TextEditingController(text: "");
  TextEditingController usernameController = TextEditingController(text: "");
  TextEditingController emailController = TextEditingController(text: "");
  TextEditingController passwordController = TextEditingController(text: "");

  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);

    handleSignUp() async {
      setState(() {
        isLoading = true;
      });

      if (await authProvider.register(
        name: nameController.text,
        username: usernameController.text,
        email: emailController.text,
        password: passwordController.text,
      )) {
        Navigator.pushNamed(context, '/home');
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: alertcolor,
            content: Text(
              'Gagal Register!',
              textAlign: TextAlign.center,
            ),
          ),
        );
      }

      setState(() {
        isLoading = false;
      });
    }

    Widget fullusernameinput() {
      return Container(
        margin: EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Nama Lengkap",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              height: 50,
              padding: EdgeInsets.symmetric(
                horizontal: 16,
              ),
              decoration: BoxDecoration(
                  color: primarytextcolor,
                  borderRadius: BorderRadius.circular(12)),
              child: Center(
                child: Row(
                  children: [
                    Icon(
                      Icons.person_outlined,
                      color: secondarycolor,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Expanded(
                        child: TextFormField(
                          controller: nameController,
                          decoration: InputDecoration.collapsed(
                              hintText: "Masukkan Nama Lengkap anda",
                              hintStyle: abuabustyle),
                        ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget usernameinput() {
      return Container(
        margin: EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Username",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              height: 50,
              padding: EdgeInsets.symmetric(
                horizontal: 16,
              ),
              decoration: BoxDecoration(
                  color: primarytextcolor,
                  borderRadius: BorderRadius.circular(12)),
              child: Center(
                child: Row(
                  children: [
                    Icon(
                      Icons.account_circle_outlined,
                      color: secondarycolor,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Expanded(
                        child: TextFormField(
                          controller: usernameController,
                          decoration: InputDecoration.collapsed(
                              hintText: "Masukkan Username anda",
                              hintStyle: abuabustyle),
                        ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget emailinput() {
      return Container(
        margin: EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Email",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              height: 50,
              padding: EdgeInsets.symmetric(
                horizontal: 16,
              ),
              decoration: BoxDecoration(
                  color: primarytextcolor,
                  borderRadius: BorderRadius.circular(12)),
              child: Center(
                child: Row(
                  children: [
                    Icon(
                      Icons.email_outlined,
                      color: secondarycolor,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Expanded(
                        child: TextFormField(
                          controller: emailController,
                          decoration: InputDecoration.collapsed(
                              hintText: "Masukkan Email anda",
                              hintStyle: abuabustyle),
                        ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget passwordinput() {
      return Container(
        margin: EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Passsword",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              height: 50,
              padding: EdgeInsets.symmetric(
                horizontal: 16,
              ),
              decoration: BoxDecoration(
                  color: primarytextcolor,
                  borderRadius: BorderRadius.circular(12)),
              child: Center(
                child: Row(
                  children: [
                    Icon(
                      Icons.lock_outlined,
                      color: secondarycolor,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Expanded(
                        child: TextFormField(
                          obscureText: true,
                          controller: passwordController,
                          decoration: InputDecoration.collapsed(
                              hintText: "Masukkan Password Anda",
                              hintStyle: abuabustyle),
                        ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget loginbutton() {

      return Container(
          height: 50,
          width: double.infinity,
          margin: EdgeInsets.only(top: 30),
          child: TextButton(
            onPressed: () {
              Navigator.pushNamed(context, "/home");
            },
            style: TextButton.styleFrom(
                backgroundColor: secondarycolor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                )),
            child: GestureDetector(
              onTap: handleSignUp,
              child: Text(
                "Register",
                style: TextStyle(
                  color: Color(0xffFFFFFF),
                  fontSize: 16,
                ),
              ),
            ),
          ));
    }

    Widget footer() {
      return Container(
        margin: EdgeInsets.only(bottom: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Sudah memiliki akun ? ",
              style: abuabustyle.copyWith(fontSize: 12),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, "/login");
              },
              child: Text(
                "Login",
                style: warnautama.copyWith(
                    fontSize: 12, fontWeight: FontWeight.w800),
              ),
            )
          ],
        ),
      );
    }

    Widget header() {
      return SafeArea(
        child: Container(
          margin: EdgeInsets.only(top: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Register",
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 2,
              ),
              Text(
                "Silahkan Daftar untuk melanjutkan",
                style: TextStyle(color: secondarytextcolor),
              )
            ],
          ),
        ),
      );
    }

    return Scaffold(
      body: SafeArea(
        child: Container(
        margin: EdgeInsets.symmetric(
        horizontal: 10,
      ),
        child: ListView(
          children: [
            header(),
            fullusernameinput(),
            usernameinput(),
            emailinput(),
            passwordinput(),
            isLoading ? LoadingButton() : loginbutton(),
            SizedBox(
              height: 50,
            ),
            footer()
          ],
        ),
      ),
    ));
  }
}


