import 'package:flutter/material.dart';
import 'package:onlineshop/providers/auth_provider.dart';
import 'package:provider/provider.dart';
import 'package:onlineshop/home/detail_chat.dart';
import 'package:onlineshop/home/mainpage.dart';
import 'package:onlineshop/login/login.dart';
import 'package:onlineshop/page/cart_page.dart';
import 'package:onlineshop/page/checkout_page.dart';
import 'package:onlineshop/page/checkout_success.dart';
import 'package:onlineshop/page/edit_profile.dart';
import 'package:onlineshop/page/product_page.dart';
import 'package:onlineshop/register/register.dart';
import 'package:onlineshop/splash/splash_screen.dart';
import 'theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => AuthProvider())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        routes: {
          "/": (context) =>SplashScreen(),
          "/login": (context) =>LoginScreen(),
          "/register": (context) =>RegisterScreen(),
          "/home": (context) =>MainPage(),
          "/detail-chat": (context) =>DetailChat(),
          "/editprofile": (context) =>EditProfilePage(),
          "/product": (context) =>ProductPage(),
          "/cart": (context) =>CartPage(),
          "/checkout": (context) =>CheckoutPage(),
          "/checkout-success": (context) =>CheckoutSuccessPage(),

        },
      ),
    );
  }
}
