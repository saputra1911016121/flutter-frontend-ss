import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:onlineshop/model/user_model.dart';
import 'package:http/http.dart' as http;

class AuthService {

  String baseUrl = "http://103.132.52.19/api/login";

  Future<UserModel> register({
    required String name,
    required String username,
    required String email,
    required String password,
}) async {
    var uri = Uri.parse("$baseUrl/register");
    var headers = {"Content-Type": "application/json"};
    var body = jsonEncode({
      "name" : name,
      "username" : username,
      "email" : email,
      "password" : password,
    });

    var response = await http.post(
        uri,
        headers: headers,
        body: body
    );

    print(response.body);

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body) ["data"];
      UserModel user = UserModel.fromJson(data["user"]);
      user.token = "Bearer " + data["access_token"];

      return user;
    } else {
      throw Exception("Gagal Register");
    }
  }
}
