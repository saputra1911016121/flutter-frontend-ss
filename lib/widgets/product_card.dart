import 'package:flutter/material.dart';

import '../theme.dart';

class ProductCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/product");
      },
      child: Container(
        width: 215,
        height: 278,
        margin: EdgeInsets.only(
          right: defaultMargin
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: sepertiabuabu
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30,),
            Image.asset(
              "assets/sepatu.png",
              width: 215,
              height: 150,
              fit: BoxFit.cover,
            ),
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: 20
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Lari",
                  style: abuabustyle.copyWith(
                    fontSize: 12
                  ),),
                  SizedBox(height: 6,),
                  Text("SEPATU NIKE SUPER DUPER",
                  style: warnahitamstyle.copyWith(
                    fontSize: 18,
                    fontWeight: semibold
                  ),
                  overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  Text(
                    "Rp. 100.000",
                    style: pricetextstyle.copyWith(
                      fontSize: 14,
                      fontWeight: medium
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

