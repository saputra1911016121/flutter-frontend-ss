import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';

class CartCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: defaultMargin,
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 10
      ),
      decoration: BoxDecoration(
        color: sepertiabuabu,
        borderRadius: BorderRadius.circular(12)
      ),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                width: 60,
                height: 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  image: DecorationImage(
                    image: AssetImage(
                      "assets/sepatu.png"
                    )
                  )
                ),
              ),
              SizedBox(
                width: 12,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Sepatu Nike"
                    ),
                    Text(
                      "Rp.100.000",
                      style: pricetextstyle,
                    )
                  ],
                ),
              ),
              Column(
                children: [
                  Image.asset(
                    "assets/additem.png",
                    width: 16,
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    "2",
                    style: warnahitamstyle.copyWith(
                      fontWeight: medium
                    ),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Image.asset(
                    "assets/menitem.png",
                    width: 16,
                  )
                ],
              )
            ],
          ),
          SizedBox(
            height: 12,
          ),
          Row(
            children: [
              Icon(
                Icons.delete_outlined,
                color: alertcolor,
              ),
              Text(
                "Hapus",
                style: alertstyle.copyWith(
                  fontWeight: light
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
