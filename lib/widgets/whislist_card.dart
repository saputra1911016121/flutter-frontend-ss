import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';

class WishListCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 20
      ),
      padding: EdgeInsets.only(
        top: 10,
        left: 12,
        bottom: 14,
        right: 20
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: sepertiabuabu
      ),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: Image.asset(
              "assets/sepatu.png",
              width: 60,
            ),
          ),
          SizedBox(
            width: 12,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Sepatu Nike",
                  style: warnautama.copyWith(
                    fontWeight: semibold
                  ),
                ),
                Text(
                  "Rp. 100.000",
                  style: pricetextstyle,
                )
              ],
            ),
          ),
          Image.asset(
            "assets/suka2.png",
            width: 34,
          )
        ],
      ),
    );
  }
}
