import 'package:flutter/material.dart';

import '../theme.dart';
class ProductTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/product");
      },
      child: Container(
        margin: EdgeInsets.only(
          left: defaultMargin,
          right: defaultMargin,
          bottom: defaultMargin
        ),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Image.asset("assets/sepatu.png",
              width: 120,
                height: 120,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: 12,
            ),
            Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Sepak Bola",
                style: abuabustyle.copyWith(
                  fontSize: 12
                ),
                ),
                SizedBox(height: 6,),
                Text(
                  "Adidas Predator",
                  style: warnahitamstyle.copyWith(
                    fontSize: 16,
                    fontWeight: semibold
                  ),
                ),
                SizedBox(height: 6,),
                Text("Rp. 100.000",
                style: pricetextstyle.copyWith(
                  fontWeight: medium
                ),)
              ],
            ))
          ],
        ),
      ),
    );
  }
}
