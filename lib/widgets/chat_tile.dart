import 'package:flutter/material.dart';
import 'package:onlineshop/theme.dart';
class ChatTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, "/detail-chat");
      },
      child: Container(
        margin: EdgeInsets.only(
          top: 30
        ),
        child: Column(
          children: [
            Row(
              children: [
                Image.asset("assets/logoshop.png",
                width: 54,),
                SizedBox(width: 12,),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Bahrul Store",
                      style: warnahitamstyle.copyWith(
                        fontSize: 15
                      ),),
                      Text("Hallo, bahrul", style:
                        abuabustyle.copyWith(
                          fontWeight: light,
                        ),
                      overflow: TextOverflow.ellipsis,
                      )
                    ],
                  ),
                ),
                Text(
                  "Now",
                  style: abuabustyle.copyWith(
                    fontSize: 10
                  ),
                )
              ],
            ),
            SizedBox(height: 12,),
            Divider(
              thickness: 1,
              color: Color(0xffF1F0F2),
            )
          ],
        ),
      ),
    );
  }
}
