import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:onlineshop/theme.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Widget header() {
      return SafeArea(
        child: Container(
          margin: EdgeInsets.only(top: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Login",
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 2,
              ),
              Text(
                "Silahkan Login untuk melanjutkan",
                style: TextStyle(color: secondarytextcolor),
              )
            ],
          ),
        ),
      );
    }

    Widget emailinput() {
      return Container(
        margin: EdgeInsets.only(top: 70),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Email",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              height: 50,
              padding: EdgeInsets.symmetric(
                horizontal: 16,
              ),
              decoration: BoxDecoration(
                  color: primarytextcolor,
                  borderRadius: BorderRadius.circular(12)),
              child: Center(
                child: Row(
                  children: [
                    Icon(
                      Icons.email_outlined,
                      color: secondarycolor,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Expanded(
                        child: TextFormField(
                      decoration: InputDecoration.collapsed(
                          hintText: "Masukkan Email anda",
                          hintStyle: abuabustyle),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget passwordinput() {
      return Container(
        margin: EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Passsword",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              height: 50,
              padding: EdgeInsets.symmetric(
                horizontal: 16,
              ),
              decoration: BoxDecoration(
                  color: primarytextcolor,
                  borderRadius: BorderRadius.circular(12)),
              child: Center(
                child: Row(
                  children: [
                    Icon(
                      Icons.lock_outlined,
                      color: secondarycolor,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Expanded(
                        child: TextFormField(
                      obscureText: true,
                      decoration: InputDecoration.collapsed(
                          hintText: "Masukkan Password Anda",
                          hintStyle: abuabustyle),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget loginbutton() {
      return Container(
          height: 50,
          width: double.infinity,
          margin: EdgeInsets.only(top: 30),
          child: TextButton(
            onPressed: () {
              Navigator.pushNamed(context, "/home");
            },
            style: TextButton.styleFrom(
                backgroundColor: secondarycolor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                )),
            child: Text(
              "Login",
              style: TextStyle( color: Color(0xffFFFFFF),
                fontSize: 16,
              ),
            ),
          ));
    }

    Widget footer() {
      return Container(
        margin: EdgeInsets.only(bottom: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Belum memiliki akun ? ",
              style: abuabustyle.copyWith(fontSize: 12),
            ),
            GestureDetector(
              onTap: (){
                Navigator.pushNamed(context, "/register");
              },
              child: Text(
                "Register",
                style: warnautama.copyWith(
                    fontSize: 12, fontWeight: FontWeight.w800),
              ),
            )
          ],
        ),
      );
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        margin: EdgeInsets.symmetric(
          horizontal: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            header(),
            emailinput(),
            passwordinput(),
            loginbutton(),
            Spacer(),
            footer()
          ],
        ),
      ),
    );
  }
}
