import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color primarycolor = Color(0xff6C5ECF);
Color secondarycolor = Color(0xff38ABBE);
Color alertcolor = Color(0xffED6363);
Color pricecolor = Color(0xff2C96F1);
Color backgroundcolor1 = Color(0xff150C4B);
Color backgroundcolor2 = Color(0xff463F6B);
Color backgroundcolor3 = Color(0xff463F6B);
Color primarytextcolor = Color(0xffDEDBDB);
Color secondarytextcolor = Color(0xff999999);
Color texthitam = Color(0xff000000);
Color transparentColor = Colors.transparent;
Color sepertiabuabu = Color(0xffF1F0F2);
Color putihbanget = Color(0xffFFFFFF);
Color productbackground = Color(0xffECEDEF);

double defaultMargin = 30.0;

TextStyle primarytextstyle = GoogleFonts.poppins(
  color: primarytextcolor,
);
TextStyle secondarytextstyle = GoogleFonts.poppins(color: secondarytextcolor);
TextStyle pricetextstyle = GoogleFonts.poppins(color: pricecolor);
TextStyle abuabustyle = GoogleFonts.poppins(color: secondarytextcolor);
TextStyle warnautama = GoogleFonts.poppins(color: secondarycolor);
TextStyle warnaputih = GoogleFonts.poppins(color: primarytextcolor);
TextStyle warnahitamstyle = GoogleFonts.poppins(color: texthitam);
TextStyle putihbangetstyle = GoogleFonts.poppins(color: putihbanget);
TextStyle alertstyle = GoogleFonts.poppins(color: alertcolor);
FontWeight light = FontWeight.w300;
FontWeight reguler = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
